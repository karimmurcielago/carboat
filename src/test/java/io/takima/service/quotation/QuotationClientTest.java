package io.takima.service.quotation;

import io.takima.config.CarBoatApplicationConfig;
import io.takima.model.Vehicle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.assertEquals;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CarBoatApplicationConfig.class, loader = AnnotationConfigContextLoader.class)
public class QuotationClientTest {

    private static final Double EXPECTED_QUOTE_PRICE = 35_000.0;


    @Autowired
    QuotationClient<Vehicle> quotationClient;

    @Test
    public void quote() {

        assertEquals(EXPECTED_QUOTE_PRICE, quotationClient.quote(Vehicle.builder().build()).join());
    }
}