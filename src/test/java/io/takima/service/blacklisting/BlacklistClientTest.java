package io.takima.service.blacklisting;

import io.takima.config.CarBoatApplicationConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CarBoatApplicationConfig.class, loader = AnnotationConfigContextLoader.class)
public class BlacklistClientTest {

    public static final String BLACKLISTED_REGISTER_NUMBER = "AA123AA";
    public static final String NOT_BLACKLISTED_REGISTER_NUMBER = "AA999AA";


    @Autowired
    BlacklistClient blacklistClient;

    @Test
    public void isBlackListed() {
        assertTrue(blacklistClient.isBlacklisted(BLACKLISTED_REGISTER_NUMBER).join());
    }

    @Test
    public void notBlackListed() {
        assertFalse(blacklistClient.isBlacklisted(NOT_BLACKLISTED_REGISTER_NUMBER).join());
    }

}