package io.takima.rule;

import io.takima.config.CarBoatApplicationConfig;
import io.takima.model.Ad;
import io.takima.model.CompleteAd;
import io.takima.model.Contacts;
import io.takima.rule.rules.LastNameRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CarBoatApplicationConfig.class, loader = AnnotationConfigContextLoader.class)
public class LastNameRuleTest {

    private static final String EMPTY = "";
    private static final String LOWER_THAN_2 = "a";
    private static final String EQUAL_2 = "ab";
    private static final String MORE_THAN_2 = "zbc";

    @Autowired
    LastNameRule lastNameRule;


    @Test(expected = NullPointerException.class)
    public void NULL() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().lastName(null).build()).build();
        List<String> errors = new LinkedList<>();
        lastNameRule.apply(ad, errors);
    }

    @Test(expected = IllegalArgumentException.class)
    public void EMPTY() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().lastName(EMPTY).build()).build();
        List<String> errors = new LinkedList<>();
        lastNameRule.apply(ad, errors);
    }

    @Test
    public void LOWER_THAN_2() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().lastName(LOWER_THAN_2).build()).build();
        List<String> errors = new LinkedList<>();
        lastNameRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(LastNameRule.NAME));
    }

    @Test
    public void EQUAL_2() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().lastName(EQUAL_2).build()).build();
        List<String> errors = new LinkedList<>();
        lastNameRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(LastNameRule.NAME));
    }

    @Test
    public void MORE_THAN_2() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().lastName(MORE_THAN_2).build()).build();
        List<String> errors = new LinkedList<>();
        lastNameRule.apply(ad, errors);

        assertEquals(0, errors.size());
        assertTrue(errors.isEmpty());
    }
}