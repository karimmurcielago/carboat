package io.takima.rule;

import io.takima.config.CarBoatApplicationConfig;
import io.takima.model.Ad;
import io.takima.model.CompleteAd;
import io.takima.model.Contacts;
import io.takima.rule.rules.NumberRateRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CarBoatApplicationConfig.class, loader = AnnotationConfigContextLoader.class)
public class NumberRateRuleTest {

    private static final String EMPTY = "";
    private static final String LOWER_THAN_30 = "12________@carboat.fr";
    private static final String EQUAL_30 = "123_______@carboat.fr";
    private static final String MORE_THAN_30 = "1234______@carboat.fr";


    @Autowired
    NumberRateRule numberRateRule;

    @Test(expected = NullPointerException.class)
    public void NULL() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().email(null).build()).build();
        List<String> errors = new LinkedList<>();
        numberRateRule.apply(ad, errors);
    }

    @Test(expected = IllegalArgumentException.class)
    public void EMPTY() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().email(EMPTY).build()).build();
        List<String> errors = new LinkedList<>();
        numberRateRule.apply(ad, errors);
    }

    @Test
    public void LOWER_THAN_30() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().email(LOWER_THAN_30).build()).build();
        List<String> errors = new LinkedList<>();
        numberRateRule.apply(ad, errors);

        assertEquals(0, errors.size());
        assertTrue(errors.isEmpty());
    }

    @Test
    public void EQUAL_30() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().email(EQUAL_30).build()).build();
        List<String> errors = new LinkedList<>();
        numberRateRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(NumberRateRule.NAME));
    }

    @Test
    public void MORE_THAN_30() {
        Ad ad = CompleteAd.CompleteAdBuilder.builder().contacts(Contacts.builder().email(MORE_THAN_30).build()).build();
        List<String> errors = new LinkedList<>();
        numberRateRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(NumberRateRule.NAME));
    }
}