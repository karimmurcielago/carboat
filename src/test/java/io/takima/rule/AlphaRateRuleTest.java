package io.takima.rule;

import io.takima.config.CarBoatApplicationConfig;
import io.takima.model.Ad;
import io.takima.model.Contacts;
import io.takima.rule.rules.AlphaRateRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CarBoatApplicationConfig.class, loader = AnnotationConfigContextLoader.class)
public class AlphaRateRuleTest {

    private static final String EMPTY = "";
    private static final String LOWER_THAN_70 = "123456____@carboat.fr";
    private static final String EQUAL_70 = "1234567___@carboat.fr";
    private static final String MORE_THAN_70 = "12345678__@carboat.fr";

    @Autowired
    AlphaRateRule alphaRateRule;

    @Test(expected = NullPointerException.class)
    public void NULL() {
        Ad ad = Ad.AdBuilder.builder().contacts(Contacts.builder().email(null).build()).build();
        List<String> errors = new LinkedList<>();
        alphaRateRule.apply(ad, errors);
    }

    @Test(expected = IllegalArgumentException.class)
    public void EMPTY() {
        Ad ad = Ad.AdBuilder.builder().contacts(Contacts.builder().email(EMPTY).build()).build();
        List<String> errors = new LinkedList<>();
        alphaRateRule.apply(ad, errors);
    }

    @Test
    public void LOWER_THAN_70() {
        Ad ad = Ad.AdBuilder.builder().contacts(Contacts.builder().email(LOWER_THAN_70).build()).build();
        List<String> errors = new LinkedList<>();
        alphaRateRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(AlphaRateRule.NAME));
    }

    @Test
    public void EQUAL_70() {
        Ad ad = Ad.AdBuilder.builder().contacts(Contacts.builder().email(EQUAL_70).build()).build();
        List<String> errors = new LinkedList<>();
        alphaRateRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(AlphaRateRule.NAME));
    }

    @Test
    public void MORE_THAN_70() {
        Ad ad = Ad.AdBuilder.builder().contacts(Contacts.builder().email(MORE_THAN_70).build()).build();
        List<String> errors = new LinkedList<>();
        alphaRateRule.apply(ad, errors);

        assertEquals(0, errors.size());
        assertTrue(errors.isEmpty());
    }

}