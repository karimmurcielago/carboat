package io.takima.rule;

import io.takima.config.CarBoatApplicationConfig;
import io.takima.model.CompleteAd;
import io.takima.model.Vehicle;
import io.takima.rule.rules.BlacklistedRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CarBoatApplicationConfig.class, loader = AnnotationConfigContextLoader.class)
public class BlackListedRuleTest {

    public static final String REGISTER_NUMBER = "AAAAAAA";


    @Autowired
    BlacklistedRule blackListedRule;

    @Test
    public void BLACKLISTED() {
        CompleteAd ad = CompleteAd.CompleteAdBuilder.builder().vehicle(Vehicle.builder().registerNumber(REGISTER_NUMBER).build()).isBlackListed(true).build();
        List<String> errors = new LinkedList<>();
        blackListedRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(BlacklistedRule.NAME));

    }


    @Test
    public void NOT_BLACKLISTED() {
        CompleteAd ad = CompleteAd.CompleteAdBuilder.builder().vehicle(Vehicle.builder().registerNumber(REGISTER_NUMBER).build()).isBlackListed(false).build();
        List<String> errors = new LinkedList<>();
        blackListedRule.apply(ad, errors);

        assertEquals(0, errors.size());
        assertTrue(errors.isEmpty());
    }

}