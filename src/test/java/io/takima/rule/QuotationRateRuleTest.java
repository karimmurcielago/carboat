package io.takima.rule;

import io.takima.config.CarBoatApplicationConfig;
import io.takima.model.CompleteAd;
import io.takima.model.Vehicle;
import io.takima.rule.rules.QuotationRateRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CarBoatApplicationConfig.class, loader = AnnotationConfigContextLoader.class)
public class QuotationRateRuleTest {


    private static final Double QUOTE_PRICE = 35_000.0;
    private static final Double PRICE_PLUS_20_POURCENT = QUOTE_PRICE * 1.20;
    private static final Double PRICE_MINUS_20_POURCENT = QUOTE_PRICE * 0.80;
    private static final Double DELTA = 1_000.0;


    @Autowired
    QuotationRateRule quotationRateRule;

    @Test
    public void CHEAPER() {


        CompleteAd ad = CompleteAd.CompleteAdBuilder.builder().price(PRICE_MINUS_20_POURCENT - DELTA).vehicle(Vehicle.builder().build()).realQuotation(QUOTE_PRICE).build();
        List<String> errors = new LinkedList<>();
        quotationRateRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(QuotationRateRule.NAME));
    }

    @Test
    public void EXACT_LOW_RANGE() {
        CompleteAd ad = CompleteAd.CompleteAdBuilder.builder().price(PRICE_MINUS_20_POURCENT).vehicle(Vehicle.builder().build()).realQuotation(QUOTE_PRICE).build();
        List<String> errors = new LinkedList<>();
        quotationRateRule.apply(ad, errors);

        assertEquals(0, errors.size());
        assertTrue(errors.isEmpty());
    }


    @Test
    public void IN_BETWEEN() {
        CompleteAd ad = CompleteAd.CompleteAdBuilder.builder().price(PRICE_MINUS_20_POURCENT + DELTA).vehicle(Vehicle.builder().build()).realQuotation(QUOTE_PRICE).build();
        List<String> errors = new LinkedList<>();
        quotationRateRule.apply(ad, errors);
        assertEquals(0, errors.size());
        assertTrue(errors.isEmpty());
    }

    @Test
    public void EXACT_UPPER_RANGE() {
        CompleteAd ad = CompleteAd.CompleteAdBuilder.builder().price(PRICE_PLUS_20_POURCENT).vehicle(Vehicle.builder().build()).realQuotation(QUOTE_PRICE).build();
        List<String> errors = new LinkedList<>();
        quotationRateRule.apply(ad, errors);

        assertEquals(0, errors.size());
        assertTrue(errors.isEmpty());
    }


    @Test
    public void MORE_EXPENSIVE() {
        CompleteAd ad = CompleteAd.CompleteAdBuilder.builder().price(PRICE_PLUS_20_POURCENT + DELTA).vehicle(Vehicle.builder().build()).realQuotation(QUOTE_PRICE).build();
        List<String> errors = new LinkedList<>();
        quotationRateRule.apply(ad, errors);

        assertEquals(1, errors.size());
        assertTrue(errors.contains(QuotationRateRule.NAME));
    }


}