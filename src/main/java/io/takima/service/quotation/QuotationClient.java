package io.takima.service.quotation;

import io.takima.service.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;


@Component
public class QuotationClient<Resource> implements Service {

    private static final Double RESOURCE_DEFAULT_QUOTATION_VALUE = 35_000.0;

    private static final Logger LOGGER = LogManager.getLogger(QuotationClient.class);

    @Autowired
    ExecutorService executorService;

    public CompletableFuture<Double> quote(Resource resource) {
        return CompletableFuture.supplyAsync(
                () -> {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        LOGGER.error("Error while thread sleep. ", e);
                    }
                    return RESOURCE_DEFAULT_QUOTATION_VALUE;
                }
                , executorService);

    }
}
