package io.takima.service.blacklisting;

import com.google.common.collect.Lists;
import io.takima.service.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;


@Component
public class BlacklistClient implements Service {

    private static final Logger LOGGER = LogManager.getLogger(BlacklistClient.class);

    private static final List<String> BLACKLISTED_REGISTER_NUMBERS = Lists.newArrayList("AA123AA");


    @Autowired
    ExecutorService executorService;

    public CompletableFuture<Boolean> isBlacklisted(String registerNumber) {

        return CompletableFuture.supplyAsync(
                () -> {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        LOGGER.error("Error while thread sleep. ", e);
                    }
                    return BLACKLISTED_REGISTER_NUMBERS.contains(registerNumber);
                }
                , executorService);
    }

}
