package io.takima.service.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.takima.model.Ad;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

@Component
public class JsonService {

    private static final Logger LOGGER = LogManager.getLogger(JsonService.class);

    @Autowired
    private ObjectMapper jsonObjectMapper;

    public Optional<Ad> toAnnounce(String jsonAnnounce) {
        try {
            return Optional.ofNullable(jsonObjectMapper.readValue(jsonAnnounce, Ad.class));
        } catch (IOException e) {
            throw new IllegalStateException("Error while mapping to announce. ", e);
        }
    }

    public Optional<Ad> toAnnounce(InputStream inputStream) {
        try {
            return Optional.ofNullable(jsonObjectMapper.readValue(inputStream, Ad.class));
        } catch (IOException e) {
            throw new IllegalStateException("Error while mapping to announce. ", e);
        }
    }


    public Optional<String> toJson(Object announce) {

        try {
            return Optional.ofNullable(jsonObjectMapper.writeValueAsString(announce));
        } catch (IOException e) {
            throw new IllegalStateException("Error while mapping to Json. ", e);
        }
    }


}
