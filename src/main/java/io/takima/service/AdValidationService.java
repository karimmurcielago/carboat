package io.takima.service;

import io.takima.CarBoatApplication;
import io.takima.model.Ad;
import io.takima.model.CompleteAd;
import io.takima.model.Vehicle;
import io.takima.rule.AdRuleRegistry;
import io.takima.rule.CompleteAdRuleRegistry;
import io.takima.service.blacklisting.BlacklistClient;
import io.takima.service.quotation.QuotationClient;
import io.takima.service.validation.ValidationService;
import io.takima.validation.ValidationRequest;
import io.takima.validation.ValidationResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;


@Component
public class AdValidationService implements ValidationService<Ad> {
    private static final Logger LOGGER = LogManager.getLogger(CarBoatApplication.class);


    @Autowired
    CompleteAdRuleRegistry completeAdRuleRegistry;

    @Autowired
    AdRuleRegistry adRuleRegistry;

    @Autowired
    BlacklistClient blacklistClient;

    @Autowired
    QuotationClient<Vehicle> quotationClient;

    @Autowired
    ExecutorService executorService;

    public CompletableFuture<ValidationResponse> validate(ValidationRequest<Ad> request) {

        List<String> errors = new LinkedList<>();
        Ad ad = request.getValidatable();

        CompletableFuture<Boolean> isBlacklistedTask = blacklistClient.isBlacklisted(request.getValidatable().getVehicle().getRegisterNumber());
        CompletableFuture<Double> quoteTask = quotationClient.quote(request.getValidatable().getVehicle());

        CompletableFuture.allOf(isBlacklistedTask, quoteTask).thenAcceptAsync(aVoid -> {
            CompleteAd completeAd = CompleteAd.CompleteAdBuilder.builder().isBlackListed(isBlacklistedTask.join()).realQuotation(quoteTask.join()).build();
            BeanUtils.copyProperties(ad, completeAd);
            completeAdRuleRegistry.apply(completeAd, errors);
            adRuleRegistry.apply(ad, errors);
        }).join();


        return CompletableFuture.supplyAsync(
                () -> ValidationResponse.builder()
                        .reference(ad.getReference())
                        .scam(!errors.isEmpty())
                        .rules(errors).build()
                , executorService);
    }


}
