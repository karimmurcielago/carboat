package io.takima.service.validation;

import io.takima.service.Service;
import io.takima.validation.Validatable;
import io.takima.validation.ValidationRequest;
import io.takima.validation.ValidationResponse;

import java.util.concurrent.CompletableFuture;

public interface ValidationService<T extends Validatable> extends Service {

    CompletableFuture<ValidationResponse> validate(ValidationRequest<T> request);

}
