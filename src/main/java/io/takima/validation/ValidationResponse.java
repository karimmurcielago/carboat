package io.takima.validation;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@JsonDeserialize(builder = ValidationResponse.ValidationResponseBuilder.class)
public class ValidationResponse {
    @JsonProperty("reference")
    private String reference;
    @JsonProperty("scam")
    private boolean scam;
    @JsonProperty("rules")
    private List<String> rules;

}
