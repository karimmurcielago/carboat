package io.takima.validation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@JsonDeserialize(builder = ValidationRequest.ValidationRequestBuilder.class)
public class ValidationRequest<T extends Validatable> {
    T validatable;
}
