package io.takima.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.takima.validation.Validatable;
import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Builder
public class Vehicle implements Validatable {
    @JsonProperty
    private String make;
    @JsonProperty
    private String model;
    @JsonProperty
    private String version;
    @JsonProperty
    private String category;
    @JsonProperty
    private String registerNumber;
    @JsonProperty
    private Integer mileage;
}
