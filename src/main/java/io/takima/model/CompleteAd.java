package io.takima.model;


import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompleteAd extends Ad {
    public Boolean isBlackListed;
    public Double realQuotation;

    public static final class CompleteAdBuilder {
        public Boolean isBlackListed;
        public Double realQuotation;
        private Contacts contacts;
        private LocalDateTime creationDate;
        private Double price;
        private List<PublicationOptions> publicationOptions;
        private String reference;
        private Vehicle vehicle;

        private CompleteAdBuilder() {
        }

        public static CompleteAdBuilder builder() {
            return new CompleteAdBuilder();
        }

        public CompleteAdBuilder isBlackListed(Boolean isBlackListed) {
            this.isBlackListed = isBlackListed;
            return this;
        }

        public CompleteAdBuilder realQuotation(Double realQuotation) {
            this.realQuotation = realQuotation;
            return this;
        }

        public CompleteAdBuilder contacts(Contacts contacts) {
            this.contacts = contacts;
            return this;
        }

        public CompleteAdBuilder creationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public CompleteAdBuilder price(Double price) {
            this.price = price;
            return this;
        }

        public CompleteAdBuilder publicationOptions(List<PublicationOptions> publicationOptions) {
            this.publicationOptions = publicationOptions;
            return this;
        }

        public CompleteAdBuilder reference(String reference) {
            this.reference = reference;
            return this;
        }

        public CompleteAdBuilder vehicle(Vehicle vehicle) {
            this.vehicle = vehicle;
            return this;
        }

        public CompleteAd build() {
            CompleteAd completeAd = new CompleteAd();
            completeAd.setIsBlackListed(isBlackListed);
            completeAd.setRealQuotation(realQuotation);
            completeAd.setContacts(contacts);
            completeAd.setCreationDate(creationDate);
            completeAd.setPrice(price);
            completeAd.setPublicationOptions(publicationOptions);
            completeAd.setReference(reference);
            completeAd.setVehicle(vehicle);
            return completeAd;
        }
    }
}
