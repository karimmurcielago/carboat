package io.takima.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.takima.validation.Validatable;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@JsonPOJOBuilder(withPrefix = "")
public class Ad implements Validatable {

    @JsonProperty
    private Contacts contacts;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @JsonProperty
    private LocalDateTime creationDate;
    @JsonProperty
    private Double price;
    @JsonProperty
    private List<PublicationOptions> publicationOptions;
    @JsonProperty
    private String reference;
    @JsonProperty
    private Vehicle vehicle;

    public static final class AdBuilder {
        private Contacts contacts;
        private LocalDateTime creationDate;
        private Double price;
        private List<PublicationOptions> publicationOptions;
        private String reference;
        private Vehicle vehicle;

        private AdBuilder() {
        }

        public static AdBuilder builder() {
            return new AdBuilder();
        }

        public AdBuilder contacts(Contacts contacts) {
            this.contacts = contacts;
            return this;
        }

        public AdBuilder creationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public AdBuilder price(Double price) {
            this.price = price;
            return this;
        }

        public AdBuilder publicationOptions(List<PublicationOptions> publicationOptions) {
            this.publicationOptions = publicationOptions;
            return this;
        }

        public AdBuilder reference(String reference) {
            this.reference = reference;
            return this;
        }

        public AdBuilder vehicle(Vehicle vehicle) {
            this.vehicle = vehicle;
            return this;
        }

        public Ad build() {
            Ad ad = new Ad();
            ad.setContacts(contacts);
            ad.setCreationDate(creationDate);
            ad.setPrice(price);
            ad.setPublicationOptions(publicationOptions);
            ad.setReference(reference);
            ad.setVehicle(vehicle);
            return ad;
        }
    }
}
