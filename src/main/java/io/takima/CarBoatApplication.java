package io.takima;

import io.takima.config.CarBoatApplicationConfig;
import io.takima.model.Ad;
import io.takima.service.mapper.JsonService;
import io.takima.service.validation.ValidationService;
import io.takima.validation.ValidationRequest;
import io.takima.validation.ValidationResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

/**
 * The type Car boat application.
 */
@Component
public class CarBoatApplication {

    private static final Logger LOGGER = LogManager.getLogger(CarBoatApplication.class);


    @Autowired
    public ValidationService<Ad> announceValidationService;

    @Autowired
    public JsonService jsonService;

    @Value("classpath:model.json")
    Resource modelJson;

    public static void main(String[] args) {
        new AnnotationConfigApplicationContext(CarBoatApplicationConfig.class);
    }


    /**
     * The mainjob of application.
     */
    @PostConstruct
    private void init() {

        InputStream inputStream;
        try {
            inputStream = modelJson.getInputStream();
        } catch (IOException e) {
            throw new IllegalStateException("Error", e);
        }

        Ad ad = jsonService.toAnnounce(inputStream).orElseThrow(IllegalStateException::new);
        ValidationRequest<Ad> validationRequest = new ValidationRequest<>(ad);
        ValidationResponse result = announceValidationService.validate(validationRequest).join();
        String response = jsonService.toJson(result).orElseThrow(IllegalStateException::new);
        LOGGER.info(response);
    }


}
