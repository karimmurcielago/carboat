package io.takima.rule;

import java.util.List;

public interface ValidationRule<T> {

    Boolean apply(T validatable, List<String> errors);

}
