package io.takima.rule;

import io.takima.model.CompleteAd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CompleteAdRuleRegistry implements ValidationRule<CompleteAd> {

    @Autowired
    List<ValidationRule<CompleteAd>> validationRules;

    @Override
    public Boolean apply(CompleteAd validatable, List<String> errors) {
        return validationRules.stream().map(rule -> rule.apply(validatable, errors)).anyMatch(e -> e == Boolean.FALSE);
    }
}
