package io.takima.rule;

import io.takima.model.Ad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AdRuleRegistry implements ValidationRule<Ad> {

    @Autowired
    List<ValidationRule<Ad>> rules;

    @Override
    public Boolean apply(Ad validatable, List<String> errors) {
        return rules.stream().map(rule -> rule.apply(validatable, errors)).anyMatch(e -> e == Boolean.FALSE);
    }
}
