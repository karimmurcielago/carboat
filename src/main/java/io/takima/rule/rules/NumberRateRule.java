package io.takima.rule.rules;

import com.google.common.base.Preconditions;
import io.takima.model.Ad;
import io.takima.rule.ValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

@Component
public class NumberRateRule implements ValidationRule<Ad> {


    public static final String NAME = "number_rate";
    private static final double NUMBER_RATE_MAXIMAL_ACCEPTABLE_VALUE = 0.30;

    static Function<String, String> getEmailBase = s -> s.split("@")[0];
    static Function<String, String> removeNumeric = s -> s.replaceAll("[0-9]", "");
    static Function<String, Integer> getNumericCharOccurrences = s -> (s.length() - removeNumeric.apply(s).length());

    private void checkPreconditions(Ad ad) {
        Preconditions.checkNotNull(ad, "announce should not be null.");
        Preconditions.checkNotNull(ad.getContacts(), "announce.contacts should not be null.");
        Preconditions.checkNotNull(ad.getContacts().getEmail(), "announce.contacts.email should not be null.");
        Preconditions.checkArgument(!ad.getContacts().getEmail().isEmpty(), "announce.contacts.email should not be empty.");
    }


    public Boolean apply(Ad ad, List<String> errors) {
        checkPreconditions(ad);
        final String baseEmail = getEmailBase.apply(ad.getContacts().getEmail());
        double numberRate = (double) getNumericCharOccurrences.apply(baseEmail) / (double) baseEmail.length();
        boolean result = numberRate < NUMBER_RATE_MAXIMAL_ACCEPTABLE_VALUE;

        if (!result) {
            errors.add(NAME);
        }

        return result;

    }

}
