package io.takima.rule.rules;

import com.google.common.base.Preconditions;
import io.takima.model.Ad;
import io.takima.rule.ValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

@Component
public class AlphaRateRule implements ValidationRule<Ad> {


    public static final String NAME = "rule::alpha_rate";
    private static final double ALPHA_RATE_MINIMAL_ACCEPTABLE_VALUE = 0.70;

    private static final Function<String, String> getEmailBase = s -> s.split("@")[0];

    private static final Function<String, Long> getAlphanumericCharOccurrences = s ->
            s.chars()
                    .mapToObj(c -> Character.toString((char) c))
                    .filter(c -> c.matches("[A-Za-z0-9]"))
                    .count();


    private void checkPreconditions(Ad ad) {
        Preconditions.checkNotNull(ad, "announce should not be null.");
        Preconditions.checkNotNull(ad.getContacts(), "announce.contacts should not be null.");
        Preconditions.checkNotNull(ad.getContacts().getEmail(), "announce.contacts.email should not be null.");
        Preconditions.checkArgument(!ad.getContacts().getEmail().isEmpty(), "announce.contacts.email should not be empty.");
    }


    public Boolean apply(Ad ad, List<String> errors) throws NullPointerException {
        checkPreconditions(ad);

        String emailBase = getEmailBase.apply(ad.getContacts().getEmail());
        long alphanumericOccurrences = getAlphanumericCharOccurrences.apply(emailBase);
        double alphaRate = (double) alphanumericOccurrences / (double) emailBase.length();
        boolean isRuleSatisfied = alphaRate > ALPHA_RATE_MINIMAL_ACCEPTABLE_VALUE;


        if (!isRuleSatisfied) {
            errors.add(NAME);
        }

        return isRuleSatisfied;

    }

}



