package io.takima.rule.rules;

import com.google.common.base.Preconditions;
import io.takima.model.CompleteAd;
import io.takima.rule.ValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BlacklistedRule implements ValidationRule<CompleteAd> {


    public static final String NAME = "rule::registernumber::blacklist";

    private void checkPreconditions(CompleteAd completeAd) {
        Preconditions.checkNotNull(completeAd, "announce should not be null.");
        Preconditions.checkNotNull(completeAd.getVehicle(), "announce.vehicle should not be null.");
        Preconditions.checkNotNull(completeAd.getVehicle().getRegisterNumber(), "announce.vehicle.registerNumber should not be null.");
        Preconditions.checkArgument(!completeAd.getVehicle().getRegisterNumber().isEmpty(), "announce.vehicle.registerNumber should not be empty.");
    }


    public Boolean apply(CompleteAd completeAd, List<String> errors) {
        checkPreconditions(completeAd);

        if (completeAd.getIsBlackListed()) {
            errors.add(NAME);
        }

        return completeAd.getIsBlackListed();
    }


}
