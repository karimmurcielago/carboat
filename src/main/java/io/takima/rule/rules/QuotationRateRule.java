package io.takima.rule.rules;

import com.google.common.base.Preconditions;
import com.google.common.collect.Range;
import io.takima.model.CompleteAd;
import io.takima.rule.ValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class QuotationRateRule implements ValidationRule<CompleteAd> {


    public static final String NAME = "rule::price::quotation_rate";

    private void checkPreconditions(CompleteAd completeAd) {
        Preconditions.checkNotNull(completeAd, "announce should not be null.");
        Preconditions.checkNotNull(completeAd.getVehicle(), "announce.vehicle should not be null.");
        Preconditions.checkNotNull(completeAd.getPrice(), "announce.price should not be null.");
    }

    public Boolean apply(CompleteAd completeAd, List<String> errors) {
        checkPreconditions(completeAd);

        boolean isRuleSatisfied = Range.closed(completeAd.getRealQuotation() * 0.80, completeAd.getRealQuotation() * 1.20).contains(completeAd.getPrice());

        if (!isRuleSatisfied) {
            errors.add(NAME);
        }

        return isRuleSatisfied;
    }


}
