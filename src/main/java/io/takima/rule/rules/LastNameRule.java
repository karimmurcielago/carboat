package io.takima.rule.rules;

import com.google.common.base.Preconditions;
import io.takima.model.Ad;
import io.takima.rule.ValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LastNameRule implements ValidationRule<Ad> {


    public static final String NAME = "rule::lastname::length";
    private static final Integer MINIMAL_LENGTH = 2;

    private void checkPreconditions(Ad ad) {
        Preconditions.checkNotNull(ad, "announce should not be null.");
        Preconditions.checkNotNull(ad.getContacts(), "announce.contacts should not be null.");
        Preconditions.checkNotNull(ad.getContacts().getLastName(), "announce.contacts.lastname should not be null.");
        Preconditions.checkArgument(!ad.getContacts().getLastName().isEmpty(), "announce.contacts.lastname should not be empty.");
    }


    public Boolean apply(Ad ad, List<String> errors) {

        checkPreconditions(ad);
        boolean result = ad.getContacts().getLastName().length() > MINIMAL_LENGTH;

        if (!result) {
            errors.add(NAME);
        }

        return result;
    }
}
