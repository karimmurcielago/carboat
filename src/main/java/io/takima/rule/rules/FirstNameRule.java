package io.takima.rule.rules;

import com.google.common.base.Preconditions;
import io.takima.model.Ad;
import io.takima.rule.ValidationRule;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FirstNameRule implements ValidationRule<Ad> {


    public static final String NAME = "rule::firstname::length";
    private static final Integer MINIMAL_LENGTH = 2;


    private void checkPreconditions(Ad ad) {
        Preconditions.checkNotNull(ad, "announce should not be null.");
        Preconditions.checkNotNull(ad.getContacts(), "announce.contacts should not be null.");
        Preconditions.checkNotNull(ad.getContacts().getFirstName(), "announce.contacts.firstname should not be null.");
        Preconditions.checkArgument(!ad.getContacts().getFirstName().isEmpty(), "announce.contacts.firstname should not be empty.");
    }


    public Boolean apply(Ad ad, List<String> errors) {
        checkPreconditions(ad);
        boolean result = ad.getContacts().getFirstName().length() > MINIMAL_LENGTH;

        if (!result) {
            errors.add(NAME);
        }

        return result;

    }
}
